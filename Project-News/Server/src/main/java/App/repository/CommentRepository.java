package App.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import App.model.Comment;

@Component
public interface CommentRepository extends JpaRepository<Comment, Long> {

}
