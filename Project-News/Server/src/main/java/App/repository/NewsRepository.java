package App.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import App.model.News;

@Component
public interface NewsRepository extends JpaRepository<News, Long> {

//	@Query("SELECT n FROM News as n WHERE n.category.id = :id AND n.title LIKE :title")
	public Page<News> findByCategoryIdAndTitleContains(@Param("id") Long id, @Param("title") String title,
			Pageable page);

}
