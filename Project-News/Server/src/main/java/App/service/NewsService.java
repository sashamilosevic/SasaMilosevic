package App.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import App.model.News;
import App.repository.NewsRepository;

@Component
public class NewsService {

	@Autowired
	private NewsRepository newsRepository;

	public Page<News> findAll(Pageable page) {
		return newsRepository.findAll(page);
	}

	public News findOne(Long id) {
		return newsRepository.findOne(id);
	}

	public News save(News news) {
		return newsRepository.save(news);
	}

	public void delete(Long id) {
		newsRepository.delete(id);
	}

	public Page<News> findByCategoryIdAndTitleContains(Long id, String title, Pageable page) {
		return newsRepository.findByCategoryIdAndTitleContains(id, title, page);
	}

}
