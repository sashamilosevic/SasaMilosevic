package App.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class News {

	@Id
	@GeneratedValue
	private Long id;

	private String title;

	@ManyToOne(cascade = CascadeType.DETACH)
	private Category category;

	private String description;

	private String text;

	public News() {
		super();
	}

	public News(Long id, String title, Category category, String description, String text) {
		super();
		this.id = id;
		this.title = title;
		this.category = category;
		this.description = description;
		this.text = text;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
