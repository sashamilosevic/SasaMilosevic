package App.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import App.model.News;
import App.service.NewsService;

@RestController
public class NewsController {

	@Autowired
	private NewsService newsService;

	@GetMapping(value = "api/news")
	public ResponseEntity<Page<News>> getNewsPage(Pageable page) {
		final Page<News> vesti = newsService.findAll(page);

		return new ResponseEntity<Page<News>>(vesti, HttpStatus.OK);
	}

	@GetMapping(value = "api/news/{id}")
	public ResponseEntity<News> getNews(@PathVariable Long id) {
		final News news = newsService.findOne(id);

		if (news == null) {
			return new ResponseEntity<News>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<News>(news, HttpStatus.OK);
	}

	@PostMapping(value = "api/news")
	public ResponseEntity<News> create(@RequestBody News news) {
		final News newNews = new News();

		newNews.setTitle(news.getTitle());
		newNews.setCategory(news.getCategory());
		newNews.setDescription(news.getDescription());
		newNews.setText(news.getText());

		final News savedNews = newsService.save(newNews);
		return new ResponseEntity<News>(savedNews, HttpStatus.CREATED);
	}

	@PutMapping(value = "api/news/{id}")
	public ResponseEntity<News> update(@PathVariable Long id, @RequestBody News news) {
		final News editedNews = newsService.findOne(id);

		if (editedNews == null) {
			return new ResponseEntity<News>(HttpStatus.NOT_FOUND);
		}

		editedNews.setId(id);
		editedNews.setTitle(news.getTitle());
		editedNews.setCategory(news.getCategory());
		editedNews.setDescription(news.getDescription());
		editedNews.setText(news.getText());

		final News savedNews = newsService.save(editedNews);
		return new ResponseEntity<>(savedNews, HttpStatus.OK);
	}

	@DeleteMapping(value = "api/news/{id}")
	public ResponseEntity<News> delete(@PathVariable Long id) {
		final News news = newsService.findOne(id);
		if (news == null) {
			return new ResponseEntity<News>(HttpStatus.NOT_FOUND);
		}

		newsService.delete(id);
		return new ResponseEntity<News>(HttpStatus.OK);
	}

	@GetMapping(value = "api/news", params = "categoryId")
	public ResponseEntity<Page<News>> findByCategoryIdAndTitleContains(@RequestParam Long categoryId,
			@RequestParam String title, Pageable page) {
		final Page<News> filteredNews = newsService.findByCategoryIdAndTitleContains(categoryId, title, page);

		return new ResponseEntity<Page<News>>(filteredNews, HttpStatus.OK);
	}
	
/* znam da sam trebao da vezem komentar za vest, ali mi je onda trebala lista comments u modelu News, pa bi poremetio sve,
 * pa sam odlucio da ostavim ovako...
 *  
 * @GetMapping(value = "api/news/{id}/comments")
	public ResponseEntity<List<Comment>> getComments(@PathVariable Long id) {
		final News news = newsService.findOne(id);
		if (news == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		final List<Comment> comments = news.getComments();
		return new ResponseEntity<>(comments, HttpStatus.OK);
	}

	@PostMapping(value = "api/news/{id}/comments")
	public ResponseEntity<Comment> createComment(@PathVariable Long id, @RequestBody Comment comment) {
		final News news = newsService.findOne(id);
		if (news == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		final Comment createdComment = new Comment();
		createdComment.setMessage(comment.getMessage());
		createdComment.setNews(news);
		List<Comment> comments = news.getComments();
		comments.add(createdComment);
		newsService.save(news);
		Comment returnComment = new Comment(createdComment);
		return new ResponseEntity<>(returnComment, HttpStatus.CREATED);
	}
*/
}
