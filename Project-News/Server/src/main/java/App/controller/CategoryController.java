package App.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import App.model.Category;
import App.service.CategoryService;

@RestController
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@GetMapping(value = "api/categories")
	public ResponseEntity<List<Category>> getCategories() {
		final List<Category> categories = categoryService.findAll();

		return new ResponseEntity<List<Category>>(categories, HttpStatus.OK);
	}

}
