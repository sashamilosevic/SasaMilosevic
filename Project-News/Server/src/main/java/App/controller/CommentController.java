package App.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import App.model.Comment;
import App.service.CommentService;

@RestController
public class CommentController {

	@Autowired
	private CommentService commentService;

	@GetMapping(value = "api/comments")
	public ResponseEntity<List<Comment>> getComments() {
		final List<Comment> comments = commentService.findAll();

		return new ResponseEntity<List<Comment>>(comments, HttpStatus.OK);
	}

	@PostMapping(value = "api/comments")
	public ResponseEntity<Comment> create(@RequestBody Comment comment) {
		final Comment newComment = new Comment();

		newComment.setName(comment.getName());
		newComment.setComment(comment.getComment());

		final Comment savedComment = commentService.save(newComment);
		return new ResponseEntity<Comment>(savedComment, HttpStatus.CREATED);
	}
}
