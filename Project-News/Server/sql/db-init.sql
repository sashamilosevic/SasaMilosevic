create database test default character set UTF8;


use test;

-- insert users
-- password is 12345 (bcrypt encoded) 
insert into security_user (username, password, first_name, last_name, role) values 
	('admin', '$2y$10$LTKdhYkreNl5LEec402o3uZXP4bJ/YSbfbF0gpYTJlEPfFIQSVCvW', 'Admin', 'Admin', 'ADMINISTRATOR');
	
    
insert into category (name) values ('zivot');  
insert into category (name) values ('kultura');
insert into category (name) values ('sport');  

insert into news (description, text, title, category_id) values ('Asteroid u obliku lobanje trebalo bi da po drugi put prodje pored Zemlje', 'List navodi da bi najvecu korist od spora Hrvatske i Slovenije mogli da imaju oni poput madjarskog premijera Viktora Orbana i poljskog ultrakonzervativnog lidera Jaroslava Kacinskog, prenosi zagrebacki portal indeks.', 'Opasnost iz Svemira', 1);
insert into news (description, text, title, category_id) values ('Srpski teniser Novak Đokovic prinudjn je da odustane od turnira u Dohi', 'Djokovic je u petak objavio da se povlaci sa Mubadala egzibicionog turnira u Abu Dabiju i bilo je ocekivano da ga nece biti na prvom zvanicnom takmicenju u sezoni', 'Novak djokovic ne igra', 3);
insert into news (description, text, title, category_id) values ('Nemački list "Velt" smatra granični spor Hrvatske i Slovenije bizarnim', 'List navodi da bi najvecu korist od spora Hrvatske i Slovenije mogli da imaju oni poput madjarskog premijera Viktora Orbana i poljskog ultrakonzervativnog lidera Jaroslava Kacinskog, prenosi zagrebacki portal indeks.

', 'Velt: HR u svadji sa svim susedima, "eksploziv ispod EU"', 2);
insert into news (description, text, title, category_id) values ('Bivsi gradonacelnik Beograda i bivši lider Demokratske stranke Dragan Djilas ocenio je da su predstojeci izbori u Beogradu najvazniji izbori posle 5. oktobra', 'On je pozvao gradjane da u danu izbora ne ostanu kod kuce vec da glasaju.

Sve promene kretale su od Beograda i moguce je, bez obzira sta vlast bude uradila, da opozicija na ovim izborima pobedi, kazao je Djilas za praznično izdanje lista Danas.

', 'Djilas: Sve promene su kretale od Beograda', 1);
insert into news (description, text, title, category_id) values ('Milos Teodosic i Bogdan Bogdanovic jedini su srpski igraci koji su nastupali protekle noci u NBA.', 'Sakramento Kingsi porazeni su na svom parketu od Finiks Sansa 111:101.
Sansi su bolje zapoceli mec, da bi Kingsi dva minuta pre kraja uspeli da smanje na samo pola kosa razlike, ali nisu imali snage za preokret.', 'Klipersima derbi u LA,Teo 11-7-5, Bogdan 13p', 3);
insert into news (description, text, title, category_id) values ('Kosarkasi CSKA iz Moskve savladali su Baskoniju u gostima', 'CSKA se mucio u Vitoriji, ali je na kraju bio koncetrisaniji u finisu i slavio rezultatom 90:81.
Domacin je mnogo bolje otvorio mec, dobila je Baskonija prvu cetvrtinu rezultatom 26:18', 'EL: CSKA u finisu slomio otpor Baskonije', 3);
insert into news (description, text, title, category_id) values ('Inspektor i fudbaler, Bata i Dzoni, covek koga vreme nije pregazilo i lik iz danasnjice. ', 'Andrija Kuzmanovic, rodjeni Beogradjanin, iako je gradio karijeru fudalera, bio je resen da se bavi glumom. Fakultet dramskih umetnosti upisao je iz drugog puta.', 'Glumac iz drugog pokusaja: Nestvarna prica koja mu je otvorila vrata glume', 2);

	
