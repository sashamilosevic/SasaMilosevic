import { Category } from "./category";

export interface News {
    id?: number;
    title: string;
    category: Category|null;
    description: string;
    text: string;


}