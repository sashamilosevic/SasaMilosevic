import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Page } from '../model/page';
import { Category } from '../model/category';

@Injectable()
export class CategoryService {

  readonly path = 'api/categories';
  
  constructor(private httpClient: HttpClient) { }
  
  getCategories(): Observable<Category[]> {
    return this.httpClient.get(this.path) as Observable<Category[]>;
  }

}
