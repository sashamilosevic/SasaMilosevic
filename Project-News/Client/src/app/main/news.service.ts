import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Page } from '../model/page';
import { News } from '../model/news';

@Injectable()
export class NewsService {

  readonly path = 'api/news';

  constructor(private httpClient: HttpClient) { }

  getAllNews(page:number): Observable<Page<News>> {
    let params = new HttpParams();
    params = params.append('page',page.toString());

    return this.httpClient.get(this.path, {params:params}) as Observable<Page<News>>;
  }

  getNews(id:number): Observable<News> {
    return this.httpClient.get(this.path) as Observable<News>;
  }

  deleteNews(news: News) {
    return this.httpClient.delete(this.path + '/' + news.id);
  }

  saveNews(news:News):Observable<News>{
    let params = new HttpParams();
    params = params.append('Content-Type', 'application/json');
    if(news.id===undefined){
      return this.httpClient.post(this.path, news, {params}) as Observable<News>;
    }
    else{
      return this.httpClient.put(this.path+'/'+news.id, news, {params}) as Observable<News>;      
    }
  }

  getNewsByCategoryIdAndTitle(id: number, title: string, page:number): Observable<Page<News>> {
    let params = new HttpParams();
    params = params.append('page',page.toString());

    return this.httpClient.get(this.path + '?categoryId=' + id + '&title=' + title, {params:params}) as Observable<Page<News>>;
  }

}
