import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { Page } from '../model/page';
import { News } from '../model/news';
import { Category } from '../model/category';
import { NewsService } from '../main/news.service';
import { CategoryService } from '../main/category.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  page:Page<News>;
  
  currentPageNumber:number;

  categories: Category[];

  forEdit: News;

  filterNews: News;

  newsTitle: string;

  constructor(private newsService: NewsService, private categoryService: CategoryService,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.currentPageNumber = 0;
    this.loadData();
    this.categoryService.getCategories().subscribe(categories => {
      this.categories = categories;
    });
    this.forEdit = {
      title: '',
      category: null,
      description: '',
      text: ''
    };
    this.filterNews = {
      title: '',
      category: null,
      description: '',
      text: ''
    };
  }

  loadData() {
    this.newsService.getAllNews(this.currentPageNumber).subscribe(data => {
      this.page = data;
    })
  }

  changePage(i:number){
    this.currentPageNumber+=i;
    this.loadData();
  }

  delete(news: News){
    this.newsService.deleteNews(news).subscribe(
      () => {
        this.loadData();
      }
    );    
  }

  add(news: News) {
    this.newsService.saveNews(news).subscribe(
      (savedItem) => {
        this.loadData();
      }
    )
  }

  edit(news: News){
    //kopija objekta comp
    this.forEdit = {...news};
  }

  isLoggedIn(): boolean {
    return this.authenticationService.isLoggedIn();
  }

  filter() {
    this.newsService.getNewsByCategoryIdAndTitle(this.filterNews.category.id, this.newsTitle, this.currentPageNumber).subscribe(data => {
      this.page = data;
    });
  }

}
