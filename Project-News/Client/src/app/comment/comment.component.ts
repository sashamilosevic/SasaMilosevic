import { Component, OnInit } from '@angular/core';
import { Comment } from '../model/comment';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  comment: Comment;

  comments: Comment[];

  private id: number;


  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.comment = {
      name: '',
      comment: ''
    };
    this.loadData();
  }

  loadData(){
    this.route.params.subscribe(param => {
      this.id = +param['id'];
      this.http.get(`api/comments`).subscribe(
        (res: any) => {
          this.comments = res;
          console.log(res);
        }
      )
    })
  }

  addComment() {
    this.http.post(`api/comments`, this.comment).subscribe(
      (res: Response) => {
        this.resetComment();
        this.loadData();
      })
  }

  resetComment(){
    this.comment = {
      name: '',
      comment: ''
    };
  }

}
