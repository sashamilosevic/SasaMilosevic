import { Component, OnInit } from '@angular/core';
import { News } from '../model/news';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Http, Response, RequestOptions, 
  Headers, URLSearchParams } from '@angular/http';

@Component({
  selector: 'app-preview-news',
  templateUrl: './preview-news.component.html',
  styleUrls: ['./preview-news.component.css']
})
export class PreviewNewsComponent implements OnInit {

  news: News;
  private id: number;

  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.route.params.subscribe(param => {
      this.id = +param['id'];
      this.http.get(`api/news/${this.id}`).subscribe(
        (res: any) => {
          this.news = res;
        }
      )
    })
  }

}
