import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { News } from '../model/news';
import { Category } from '../model/category';
import { NewsService } from '../main/news.service';
import { CategoryService } from '../main/category.service';


@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.css']
})
export class AddNewsComponent implements OnInit {

  @Input()
  news: News;

  @Input()
  categories: Category[];

  @Output()
  addNews: EventEmitter<News> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  add() {
    this.addNews.next(this.news);
    this.news = {
      title: '',
      category: null,
      description: '',
      text: ''
    }
  }

  reset() {
    this.news = {
      title: '',
      category: null,
      description: '',
      text: ''
    }
  }

  byId(category1: Category, category2:Category){
    if(category1 && category2){
      return category1.id===category2.id;
    }
  }

}
