create database praksa default character set UTF8;


use praksa;

insert into security_user (username, password, first_name, last_name, image_path, email, locked, role) values 
	('admin', '$2a$04$CWEwRQxohY7TM7w93NER3OHOsmJ/XZRPYZ8JqxpglSXd90RMEYPzm', 'Admin', 'Admin', 'https://i.pinimg.com/736x/2c/9d/07/2c9d0704ae49dfde914e2b477bf9279c--stick-figure-profile-pictures.jpg','admin@gmail.com', false, 'ADMINISTRATOR');
insert into security_user (username, password, first_name, last_name, image_path, email, locked, role) values 
	('sasa', '$2a$04$zvmab1CiuwpNgaOcSQyh1Oy4ITIwZa3S1X503I2lVhK5a4LsHrZDi', 'Sasa', 'Milosevic', 'http://sguru.org/wp-content/uploads/2017/06/cool-anonymous-profile-pictures-1699946_orig.jpg', 'sashamilosevic@yahoo.com', false, 'USER');
insert into security_user (username, password, first_name, last_name, image_path, email, locked, role) values 
	('petar', '$2a$04$hP2u8V/pvRtsnJWj0jxFve3V.aQ.sAzgO71maXu/0fA/VYOY2QSda', 'Petar', 'Petrovic', 'https://i.pinimg.com/736x/73/8b/82/738b82ae3c1a1b793aa9a68d9b19439f--funny-profile-pictures-profile-pics.jpg','petar.petrovic@yahoo.com', false, 'USER');
insert into security_user (username, password, first_name, last_name, image_path, email, locked, role) values 
	('marko', '$2a$04$Y5FDmYfAXw8P0nKskPOGmew1sWRMyLjJ0fb1oKkx0N87SDZhhCl2.', 'Marko', 'Markovic', 'http://trendsinusa.com/wp-content/uploads/2018/01/df68f8a4f8e01eb6e93973c9db5eb7a8.jpg','makovic017@gmail.com', false, 'USER');
	
    
drop database praksa;