package App.security;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import App.model.Role;
import App.model.SecurityUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenUtils {

	@Value("myXAuthSecret")
	private String secret;

	@Value("18000") // in seconds (5 hours)
	private Long expiration;

	public String getUsernameFromToken(String token) {
		try {
			final Claims claims = this.getClaimsFromToken(token);
			return claims.getSubject();
		} catch (Exception e) {
			return null;
		}
	}

	private Claims getClaimsFromToken(String token) {
		try {
			final Claims claims = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).getBody();
			return claims;
		} catch (Exception e) {
			return null;
		}
	}

	public Date getExpirationDateFromToken(String token) {
		try {
			final Claims claims = this.getClaimsFromToken(token);
			return claims.getExpiration();
		} catch (Exception e) {
			return null;
		}
	}

	private boolean isTokenExpired(String token) {
		final Date expiration = this.getExpirationDateFromToken(token);
		return expiration.before(new Date(System.currentTimeMillis()));
	}

	public boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsernameFromToken(token);
		return username.equals(userDetails.getUsername()) && !isTokenExpired(token);
	}

	public String generateToken(UserDetails userDetails) {
		final Map<String, Object> claims = new HashMap<>();
		claims.put("roles", userDetails.getAuthorities());
		claims.put("sub", userDetails.getUsername());
		claims.put("created", new Date(System.currentTimeMillis()));

		return Jwts.builder().setClaims(claims).setExpiration(new Date(System.currentTimeMillis() + expiration * 1000))
				.signWith(SignatureAlgorithm.HS512, secret).compact();
	}
	
	public String generateToken(SecurityUser user) {
		final Map<String, Object> claims = new HashMap<>();
		claims.put("roles", user.getRole());
		claims.put("sub", user.getUsername());
		claims.put("created", new Date(System.currentTimeMillis()));

		return Jwts.builder().setClaims(claims).setExpiration(new Date(System.currentTimeMillis() + expiration * 1000))
				.signWith(SignatureAlgorithm.HS512, secret).compact();
	}
	
	public SecurityUser parseToken(String token) {
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            SecurityUser u = new SecurityUser();
            u.setId(Long.parseLong((String) body.get("id")));
            u.setUsername(body.getSubject());
            u.setPassword((String) body.get("password"));
            u.setFirstName((String) body.get("firstName"));
            u.setLastName((String) body.get("lastName"));
            u.setImagePath((String) body.get("imagePath"));
            u.setEmail((String) body.get("email"));
            u.setRole((Role) body.get("role"));

            return u;

        } catch (JwtException | ClassCastException e) {
            return null;
        }
    }

}
