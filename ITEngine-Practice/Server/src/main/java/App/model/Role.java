package App.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Role {
	@JsonProperty("ADMINISTRATOR")
	ADMINISTRATOR, @JsonProperty("USER")
	USER
}
