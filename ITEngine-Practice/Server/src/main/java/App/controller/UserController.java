package App.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import App.dto.LoginDTO;
import App.dto.TokenDTO;
import App.dto.UserProfileDTO;
import App.model.SecurityUser;
import App.security.TokenUtils;
import App.service.UserDetailsServiceImpl;
import App.util.Crypt;
import App.util.SmtpMailSender;

@RestController
public class UserController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private UserDetailsServiceImpl userService;

	@Autowired
	TokenUtils tokenUtils;

	@Autowired
	private Environment env;

	@Autowired
	private SmtpMailSender smtpMailSender;

	@PostMapping(value = "/api/login")
	public ResponseEntity<TokenDTO> login(@RequestBody LoginDTO loginDTO) {
		try {
			final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
					loginDTO.getUsername(), loginDTO.getPassword());
			final Authentication authentication = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			final UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getUsername());
			final String genToken = tokenUtils.generateToken(details);
			final SecurityUser user = userService.findByUsername(loginDTO.getUsername())
					.orElseThrow(() -> new UsernameNotFoundException(
							String.format("No user found with username '%s'.", loginDTO.getUsername())));
			if (user.isLocked() == false) {
				return new ResponseEntity<>(new TokenDTO(genToken), HttpStatus.OK);
			}
		} catch (Exception ex) {

		}
		return new ResponseEntity<>(new TokenDTO(""), HttpStatus.BAD_REQUEST);
	}

	@PostMapping(value = "/api/user")
	public ResponseEntity<UserProfileDTO> registerUser(@RequestBody SecurityUser user) {

		// final String genToken = tokenUtils.generateToken(user);

		user.setLocked(true);
		userService.addUser(user);
		UserProfileDTO createdUser = new UserProfileDTO(user);

		try {
			smtpMailSender.send(user.getEmail(), "IT Engine - Please accept invitation",
					"Click the following ling to verify your account:"
							+ " https://localhost:8443/confirmationRegistration/" + user.getId());
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

		return new ResponseEntity<UserProfileDTO>(createdUser, HttpStatus.CREATED);
	}

	@GetMapping(value = "/confirmationRegistration/{id}")
	public ResponseEntity<UserProfileDTO> confirmRegistration(@PathVariable Long id) {
		// String username = tokenUtils.getUsernameFromToken(token);
		// final SecurityUser user = userService.findByUsername(username).orElseThrow(
		// () -> new UsernameNotFoundException(String.format("No user found with
		// username '%s'.", username)));
		SecurityUser user = userService.findOne(id);
		user.setPassword(Crypt.hashPassword(user.getPassword()));
		user.setLocked(false);
		userService.addUser(user);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", "https://localhost:8443/login");
		return new ResponseEntity<UserProfileDTO>(headers, HttpStatus.OK);
	}

	private SimpleMailMessage constructEmail(String subject, String body, SecurityUser user) {
		final SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject(subject);
		email.setText(body);
		email.setTo(user.getEmail());
		email.setFrom(env.getProperty("support.email"));
		return email;
	}

	@GetMapping(value = "api/users")
	public ResponseEntity<List<UserProfileDTO>> findAll() {
		List<UserProfileDTO> usersDTO;
		usersDTO = userService.findAll().stream().map(UserProfileDTO::new).collect(Collectors.toList());
		return new ResponseEntity<List<UserProfileDTO>>(usersDTO, HttpStatus.OK);
	}

	@DeleteMapping(value = "api/users/{id}")
	public ResponseEntity remove(@PathVariable Long id) {
		final SecurityUser user = userService.findOne(id);
		if (user == null) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		} else {
			userService.remove(id);
			return new ResponseEntity(HttpStatus.OK);
		}
	}

	@GetMapping(value = "api/users/{id}")
	public ResponseEntity<UserProfileDTO> findOne(@PathVariable Long id) {
		SecurityUser user = userService.findOne(id);

		if (user == null) {
			return new ResponseEntity<UserProfileDTO>(HttpStatus.NOT_FOUND);
		} else {
			UserProfileDTO savedUser = new UserProfileDTO(user);
			return new ResponseEntity<UserProfileDTO>(savedUser, HttpStatus.OK);
		}
	}

	@PutMapping(value = "api/users/{id}")
	public ResponseEntity<UserProfileDTO> update(@PathVariable Long id, @RequestBody SecurityUser user) {
		final SecurityUser editedUser = userService.findOne(id);
		if (user == null) {
			return new ResponseEntity<UserProfileDTO>(HttpStatus.NOT_FOUND);
		} else {
			editedUser.setFirstName(user.getFirstName());
			editedUser.setLastName(user.getLastName());
			editedUser.setUsername(user.getUsername());
			editedUser.setPassword(Crypt.hashPassword(user.getPassword()));
			editedUser.setImagePath(user.getImagePath());
			editedUser.setRole(user.getRole());
			userService.addUser(editedUser);
			UserProfileDTO savedUser = new UserProfileDTO(editedUser);

			return new ResponseEntity<UserProfileDTO>(savedUser, HttpStatus.CREATED);
		}
	}

	@GetMapping(value = "api/users", params = "firstName")
	public ResponseEntity<List<UserProfileDTO>> findByFirstNameContains(@RequestParam String firstName) {
		List<UserProfileDTO> usersDTO;
		usersDTO = userService.findByFirstNameContains(firstName).stream().map(UserProfileDTO::new)
				.collect(Collectors.toList());
		return new ResponseEntity<List<UserProfileDTO>>(usersDTO, HttpStatus.OK);
	}
}
