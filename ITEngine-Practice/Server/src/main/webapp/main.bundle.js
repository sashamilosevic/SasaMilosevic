webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = "ul .active a{\r\n    border-right: 2px solid #333;\r\n}\r\n\r\nul .active a:hover{\r\n    color: white;\r\n    background-color: #333;\r\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar-inverse bg-inverse\">\r\n  <div class=\"container\">\r\n      <div class=\"navbar-header\">\r\n      </div>\r\n      <div id=\"navbar\" class=\"navbar-collapse collapse\">\r\n        <a class=\"navbar-brand\" href=\"main\">\r\n            <img src=\"http://www.indiaretailing.com/wp-content/uploads/2016/02/Home-Button.png\" width=\"30\" height=\"30\" alt=\"\">\r\n        </a>\r\n        <ul class=\"nav navbar-nav pull-left\">\r\n            <li *ngIf=\"!isLoggedIn()\" class=\"nav-item active\"><a href=\"register\"><img src=\"http://adaptivesportsne.org/wp-content/uploads/2015/04/Register-Button.png\" width=\"100\" height=\"30\" alt=\"\"></a></li>\r\n        </ul>\r\n        <ul class=\"nav navbar-nav pull-right\">\r\n            <li *ngIf=\"isLoggedIn()\" class=\"nav-item active\"><a (click)=\"logout()\"><img src=\"https://au.kg/images/logout.png\" width=\"30\" height=\"30\" alt=\"\"></a></li>\r\n            <li *ngIf=\"!isLoggedIn()\" class=\"nav-item active\"><a href=\"login\"><img src=\"https://www.shareicon.net/download/2016/10/11/842382_multimedia_512x512.png\" width=\"30\" height=\"30\" alt=\"\"></a></li>\r\n        </ul>\r\n      </div>\r\n  </div>\r\n</nav>\r\n<div class=\"container theme-showcase\" role=\"main\">\r\n  <div class=\"jumbotron\" style=\"padding: 30px 15px;\r\n                                margin-bottom: 30px;\r\n                                height: auto;\r\n                                color: inherit;\r\n                                background-position: center center;\r\n                                background-color: #f9f9f9;\r\n                                background-size: cover;\r\n                                background-image:url('https://wallpapercave.com/wp/wp1828955.jpg');\">\r\n      <img src=\"http://ftninformatika.com/img/partners/it_engine.png\" width=\"600\" height=\"150\" alt=\"\">\r\n      <h2 class=\"text-info font-weight-bold\" align=\"right\">Praksa - Sasha Milosevic</h2>\r\n  </div>\r\n  <router-outlet></router-outlet>\r\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var authentication_service_1 = __webpack_require__("./src/app/service/authentication.service.ts");
var AppComponent = (function () {
    function AppComponent(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
    }
    AppComponent.prototype.logout = function () {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    };
    AppComponent.prototype.isLoggedIn = function () {
        return this.authenticationService.isLoggedIn();
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
            router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var app_component_1 = __webpack_require__("./src/app/app.component.ts");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var register_component_1 = __webpack_require__("./src/app/register/register.component.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var pagenotfound_component_1 = __webpack_require__("./src/app/pagenotfound/pagenotfound.component.ts");
var login_component_1 = __webpack_require__("./src/app/login/login.component.ts");
var main_component_1 = __webpack_require__("./src/app/main/main.component.ts");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var token_interceptor_service_1 = __webpack_require__("./src/app/service/token-interceptor.service.ts");
var authentication_service_1 = __webpack_require__("./src/app/service/authentication.service.ts");
var jwt_utils_service_1 = __webpack_require__("./src/app/service/jwt-utils.service.ts");
var can_activate_auth_guard_1 = __webpack_require__("./src/app/service/can-activate-auth.guard.ts");
var user_service_1 = __webpack_require__("./src/app/main/user.service.ts");
var user_preview_component_1 = __webpack_require__("./src/app/user-preview/user-preview.component.ts");
var edit_user_component_1 = __webpack_require__("./src/app/edit-user/edit-user.component.ts");
var material_1 = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
var ng_bootstrap_1 = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var animations_1 = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
var modal_component_1 = __webpack_require__("./src/app/modal/modal.component.ts");
var ng2_toastr_1 = __webpack_require__("./node_modules/ng2-toastr/ng2-toastr.js");
var verification_message_component_1 = __webpack_require__("./src/app/verification-message/verification-message.component.ts");
var verify_component_1 = __webpack_require__("./src/app/verify/verify.component.ts");
var appRoutes = [
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'verification', component: verification_message_component_1.VerificationMessageComponent },
    { path: 'main', component: main_component_1.MainComponent, canActivate: [can_activate_auth_guard_1.CanActivateAuthGuard] },
    { path: 'users/:id', component: user_preview_component_1.UserPreviewComponent },
    { path: 'register', component: register_component_1.RegisterComponent },
    { path: 'confirmationRegistration', component: verify_component_1.VerifyComponent },
    { path: 'lazy', component: modal_component_1.ModalComponent },
    { path: '', redirectTo: 'main', pathMatch: 'full', canActivate: [can_activate_auth_guard_1.CanActivateAuthGuard] },
    { path: '**', component: pagenotfound_component_1.PagenotfoundComponent }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                register_component_1.RegisterComponent,
                pagenotfound_component_1.PagenotfoundComponent,
                login_component_1.LoginComponent,
                main_component_1.MainComponent,
                user_preview_component_1.UserPreviewComponent,
                edit_user_component_1.EditUserComponent,
                modal_component_1.ModalComponent,
                verification_message_component_1.VerificationMessageComponent,
                verify_component_1.VerifyComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpClientModule,
                forms_1.FormsModule,
                material_1.MatButtonModule,
                material_1.MatCheckboxModule,
                animations_1.BrowserAnimationsModule,
                ng_bootstrap_1.NgbModule.forRoot(),
                ng2_toastr_1.ToastModule.forRoot(),
                router_1.RouterModule.forRoot(appRoutes, {
                    enableTracing: false
                })
            ],
            entryComponents: [
                modal_component_1.ModalComponent
            ],
            providers: [
                {
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: token_interceptor_service_1.TokenInterceptorService,
                    multi: true
                },
                authentication_service_1.AuthenticationService,
                can_activate_auth_guard_1.CanActivateAuthGuard,
                jwt_utils_service_1.JwtUtilsService,
                user_service_1.UserService
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "./src/app/edit-user/edit-user.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/edit-user/edit-user.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!show\">\n  <h1 class=\"text-white\">Edit User</h1>\n\n  <form class=\"add-project\" (ngSubmit)=\"editUser(); toggle(); showInfo()\">\n    <div class=\"form-group\">\n      <label class=\"text-white\">Username</label>\n      <input type=\"text\" name=\"username\" class=\"form-control\" [(ngModel)]=\"user.username\" placeholder=\"username\"/>\n    </div>\n    <div class=\"form-group\">\n      <label class=\"text-white\">Password</label>\n      <input type=\"password\" name=\"password\" class=\"form-control\" [(ngModel)]=\"user.password\" placeholder=\"password\"/>\n    </div>\n    <div class=\"form-group\">\n      <label class=\"text-white\">Name</label>\n      <input type=\"firstName\" name=\"firstName\" class=\"form-control\" [(ngModel)]=\"user.firstName\" placeholder=\"firstname\"/>\n    </div>\n    <div class=\"form-group\">\n      <label class=\"text-white\">LastName</label>\n      <input type=\"lastName\" name=\"lastName\" class=\"form-control\" [(ngModel)]=\"user.lastName\" placeholder=\"lastname\"/>\n    </div>\n    <!-- <input type=\"file\" (change)=\"fileChange($event)\" placeholder=\"Upload file\" accept=\".jpeg,.bmp,.png,.jpg\"> -->\n    <div class=\"form-group\">\n        <label class=\"text-white\">ImageURL</label>\n        <input type=\"imagePath\" name=\"imagePath\" class=\"form-control\" [(ngModel)]=\"user.imagePath\" placeholder=\"imagePath\"/>\n      </div>\n    <br/>\n    <input class=\"btn btn-success\" type=\"submit\" value=\"Edit\"/>\n  </form>\n</div>\n\n"

/***/ }),

/***/ "./src/app/edit-user/edit-user.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var ng2_toastr_1 = __webpack_require__("./node_modules/ng2-toastr/ng2-toastr.js");
var EditUserComponent = (function () {
    function EditUserComponent(http, toastr, vcr) {
        this.http = http;
        this.toastr = toastr;
        this.editedStudent = new core_1.EventEmitter();
        this.show = false;
        this.toastr.setRootViewContainerRef(vcr);
    }
    EditUserComponent.prototype.ngOnInit = function () {
    };
    EditUserComponent.prototype.editUser = function () {
        this.editedStudent.next(this.user);
    };
    EditUserComponent.prototype.toggle = function () {
        this.show = !this.show;
    };
    EditUserComponent.prototype.alert = function () {
        alert(this.user.firstName + " " + this.user.lastName + " is changed");
    };
    EditUserComponent.prototype.showInfo = function () {
        this.toastr.info('User edited.');
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], EditUserComponent.prototype, "id", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], EditUserComponent.prototype, "user", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], EditUserComponent.prototype, "editedStudent", void 0);
    EditUserComponent = __decorate([
        core_1.Component({
            selector: 'app-edit-user',
            template: __webpack_require__("./src/app/edit-user/edit-user.component.html"),
            styles: [__webpack_require__("./src/app/edit-user/edit-user.component.css")]
        }),
        __metadata("design:paramtypes", [http_1.HttpClient, ng2_toastr_1.ToastsManager, core_1.ViewContainerRef])
    ], EditUserComponent);
    return EditUserComponent;
}());
exports.EditUserComponent = EditUserComponent;


/***/ }),

/***/ "./src/app/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\"> \r\n  <div class=\"row\"> \r\n    <div class=\"col-md-3\"></div> \r\n    <div class=\"col-md-6\"> \r\n      <form class=\"form-signin\" (ngSubmit)=\"login()\"> \r\n        <h2 class=\"form-signin-heading text-white\">Please sign in</h2> \r\n        <label for=\"username\" class=\"sr-only\">Username</label> \r\n        <input type=\"text\" id=\"username\" class=\"form-control\" name=\"username\" [(ngModel)]=\"user.username\" placeholder=\"Username\" \r\n          required autofocus> \r\n        <label for=\"inputPassword\" class=\"sr-only\">Password</label> \r\n        <input type=\"password\" id=\"inputPassword\" class=\"form-control\" name=\"username\" [(ngModel)]=\"user.password\" placeholder=\"Password\" \r\n          required> \r\n        <button class=\"btn btn-primary btn-block\" type=\"submit\">Sign in</button> \r\n      </form> \r\n      <div *ngIf=wrongUsernameOrPass class=\"alert alert-warning box-msg\" role=\"alert\"> \r\n        Wrong username or password. \r\n      </div> \r\n    </div> \r\n    <div class=\"col-md-3\"></div> \r\n  </div> \r\n</div>  "

/***/ }),

/***/ "./src/app/login/login.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var Observable_1 = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
var authentication_service_1 = __webpack_require__("./src/app/service/authentication.service.ts");
var LoginComponent = (function () {
    function LoginComponent(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
        this.user = {};
        this.wrongUsernameOrPass = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.authenticationService.login(this.user.username, this.user.password).subscribe(function (loggedIn) {
            if (loggedIn) {
                _this.router.navigate(['/main']);
            }
        }, function (err) {
            if (err.toString() === 'Ilegal login') {
                _this.wrongUsernameOrPass = true;
                console.log(err);
            }
            else {
                Observable_1.Observable.throw(err);
            }
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            template: __webpack_require__("./src/app/login/login.component.html"),
            styles: [__webpack_require__("./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
            router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;


/***/ }),

/***/ "./src/app/main/main.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/main/main.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"input-group col-md-6 col-md-offset-3\">\r\n      <input type=\"text\" class=\"form-control\" placeholder=\"Search by Title: \" aria-label=\"Recipient's username\" aria-describedby=\"basic-addon2\" [(ngModel)] = \"title\">\r\n      <div class=\"input-group-append\">\r\n        <button class=\"btn btn-outline-light\" type=\"button\" (click)=\"searchByTitle()\">Search</button>\r\n      </div>\r\n  </div>        \r\n</div>\r\n<br/>\r\n<br/>\r\n<div *ngIf=\"users\">\r\n  <div class=\"container-fluid col-md-12\">\r\n    <ul *ngFor=\"let user of users\">\r\n      <div> \r\n        <div class=\"card-transparent col-md-4 text-center border-0\">\r\n          <img class=\"card-img-top rounded\" [src]=\"getSantizeUrl(user.imagePath)\" height=\"200px\" alt=\"Card image cap\">\r\n          <div class=\"card-body\">\r\n            <h4 class=\"card-title\"><a class=\"text-white\" [routerLink]= \"['/users/', user.id]\">{{ user.firstName }}</a></h4>\r\n            <p class=\"text-white\">First Name: {{ user.firstName }}</p>\r\n            <p class=\"text-white\">Last Name: {{ user.lastName }}</p>\r\n            <p class=\"text-white\">E-mail: {{ user.email }}</p>\r\n          </div>\r\n          <br/>\r\n          <button class=\"btn btn-danger btn-block\" (click)=\"delete(user); showSuccess()\">Delete</button>\r\n          <button class=\"btn btn-warning btn-block\" (click)= \"toggle()\">Edit</button>\r\n          <br/>\r\n          <br/>\r\n          <div *ngIf=\"show\">\r\n            <app-edit-user #edit [user]=\"user\" (editedUser)=\"edit($event)\"></app-edit-user>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </ul>\r\n  </div>  \r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/main/main.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var authentication_service_1 = __webpack_require__("./src/app/service/authentication.service.ts");
var user_service_1 = __webpack_require__("./src/app/main/user.service.ts");
var platform_browser_1 = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var ng_bootstrap_1 = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var modal_component_1 = __webpack_require__("./src/app/modal/modal.component.ts");
var ng2_toastr_1 = __webpack_require__("./node_modules/ng2-toastr/ng2-toastr.js");
var MainComponent = (function () {
    function MainComponent(toastr, vcr, modalService, http, authenticationService, userService, sanitizer, router) {
        this.toastr = toastr;
        this.modalService = modalService;
        this.http = http;
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.sanitizer = sanitizer;
        this.router = router;
        this.show = false;
        this.forEdit = this.userService.getUser(this.id);
        this.image = 'C:\SasaMilosevic\Workspace\ITEngine\Client\src\assets\download.jpg)';
        this.toastr.setRootViewContainerRef(vcr);
    }
    ;
    MainComponent.prototype.ngOnInit = function () {
        this.loadData();
        // this.isAdministrator = this.authenticationService.isAdmin();
        this.trustedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.image);
    };
    MainComponent.prototype.loadData = function () {
        var _this = this;
        this.userService.getUsers().subscribe(function (data) {
            _this.users = data;
        });
    };
    MainComponent.prototype.editUser = function (user) {
        var _this = this;
        this.userService.editUser(user).subscribe(function (data) {
            _this.loadData();
        });
    };
    MainComponent.prototype.delete = function (user) {
        var _this = this;
        this.userService.deleteNews(user).subscribe(function () {
            _this.loadData();
        });
    };
    MainComponent.prototype.isLoggedIn = function () {
        return this.authenticationService.isLoggedIn();
    };
    MainComponent.prototype.getSantizeUrl = function (url) {
        return this.sanitizer.bypassSecurityTrustUrl(url);
    };
    MainComponent.prototype.toggle = function () {
        this.show = !this.show;
    };
    MainComponent.prototype.openModal = function () {
        this.modalRef = this.modalService.open(this.modal);
    };
    MainComponent.prototype.showSuccess = function () {
        this.toastr.error('User deleted.', 'Success!');
    };
    MainComponent.prototype.searchByTitle = function () {
        var _this = this;
        this.userService.findByTitle(this.title).subscribe(function (data) {
            _this.users = data;
        });
    };
    MainComponent.prototype.showInfo = function () {
        this.toastr.info('User edited.');
    };
    __decorate([
        core_1.ViewChild(modal_component_1.ModalComponent),
        __metadata("design:type", modal_component_1.ModalComponent)
    ], MainComponent.prototype, "modal", void 0);
    MainComponent = __decorate([
        core_1.Component({
            selector: 'app-main',
            template: __webpack_require__("./src/app/main/main.component.html"),
            styles: [__webpack_require__("./src/app/main/main.component.css")]
        }),
        __metadata("design:paramtypes", [ng2_toastr_1.ToastsManager, core_1.ViewContainerRef, ng_bootstrap_1.NgbModal, http_1.HttpClient, authentication_service_1.AuthenticationService, user_service_1.UserService, platform_browser_1.DomSanitizer, router_1.Router])
    ], MainComponent);
    return MainComponent;
}());
exports.MainComponent = MainComponent;


/***/ }),

/***/ "./src/app/main/user.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var UserService = (function () {
    function UserService(httpClient) {
        this.httpClient = httpClient;
        this.path = 'api/users';
    }
    UserService.prototype.getUsers = function () {
        return this.httpClient.get(this.path);
    };
    UserService.prototype.getUser = function (id) {
        return this.httpClient.get(this.path + '/' + id);
    };
    UserService.prototype.editUser = function (user) {
        return this.httpClient.put(this.path + '/' + user.id, user);
    };
    UserService.prototype.deleteNews = function (user) {
        return this.httpClient.delete(this.path + '/' + user.id);
    };
    UserService.prototype.findByTitle = function (title) {
        return this.httpClient.get(this.path + '?firstName=' + title);
    };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;


/***/ }),

/***/ "./src/app/modal/modal.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modal/modal.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-template #content let-c=\"close\" let-d=\"dismiss\">\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title\">Attention</h4>\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <p>Are you sure you want to delete this?&hellip;</p>\n  </div>\n  <div class=\"modal-footer\">\n    <button type=\"button\" class=\"btn btn-danger btn-outline-dark\" (click)=\"delete()\">Confirm</button>\n    <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"c('Close click')\">Cancel</button>\n  </div>\n</ng-template>\n\n"

/***/ }),

/***/ "./src/app/modal/modal.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var ng_bootstrap_1 = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var ModalComponent = (function () {
    function ModalComponent(modalService) {
        this.modalService = modalService;
        this.userDeleted = new core_1.EventEmitter();
    }
    ModalComponent.prototype.ngOnInit = function () {
    };
    ModalComponent.prototype.delete = function () {
        this.userDeleted.next(this.user);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ModalComponent.prototype, "user", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ModalComponent.prototype, "userDeleted", void 0);
    ModalComponent = __decorate([
        core_1.Component({
            selector: 'app-modal',
            template: __webpack_require__("./src/app/modal/modal.component.html"),
            styles: [__webpack_require__("./src/app/modal/modal.component.css")]
        }),
        __metadata("design:paramtypes", [ng_bootstrap_1.NgbModal])
    ], ModalComponent);
    return ModalComponent;
}());
exports.ModalComponent = ModalComponent;


/***/ }),

/***/ "./src/app/pagenotfound/pagenotfound.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pagenotfound/pagenotfound.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>\r\n  Page not found.\r\n</h1>"

/***/ }),

/***/ "./src/app/pagenotfound/pagenotfound.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var PagenotfoundComponent = (function () {
    function PagenotfoundComponent() {
    }
    PagenotfoundComponent.prototype.ngOnInit = function () {
    };
    PagenotfoundComponent = __decorate([
        core_1.Component({
            selector: 'app-pagenotfound',
            template: __webpack_require__("./src/app/pagenotfound/pagenotfound.component.html"),
            styles: [__webpack_require__("./src/app/pagenotfound/pagenotfound.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PagenotfoundComponent);
    return PagenotfoundComponent;
}());
exports.PagenotfoundComponent = PagenotfoundComponent;


/***/ }),

/***/ "./src/app/register/register.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<h1 class=\"text-white\">Register</h1>\r\n\r\n<form class=\"add-project\" (ngSubmit)=\"addUser()\">\r\n  <div class=\"form-group\">\r\n    <label class=\"text-white\">Username</label>\r\n    <input type=\"text\" name=\"username\" class=\"form-control\" [(ngModel)]=\"userToAdd.username\" placeholder=\"username\"/>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label class=\"text-white\">Password</label>\r\n    <input type=\"password\" name=\"password\" class=\"form-control\" [(ngModel)]=\"userToAdd.password\" placeholder=\"password\"/>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label class=\"text-white\">Name</label>\r\n    <input type=\"firstName\" name=\"firstName\" class=\"form-control\" [(ngModel)]=\"userToAdd.firstName\" placeholder=\"firstname\"/>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label class=\"text-white\">LastName</label>\r\n    <input type=\"lastName\" name=\"lastName\" class=\"form-control\" [(ngModel)]=\"userToAdd.lastName\" placeholder=\"lastname\"/>\r\n  </div>\r\n  <!-- <input type=\"file\" (change)=\"fileChange($event)\" placeholder=\"Upload file\" accept=\".jpeg,.bmp,.png,.jpg\"> -->\r\n  <div class=\"form-group\">\r\n    <label class=\"text-white\">E-mail</label>\r\n    <input type=\"email\" name=\"email\" class=\"form-control\" [(ngModel)]=\"userToAdd.email\" placeholder=\"email\"/>\r\n  </div>\r\n  <div class=\"form-group\">\r\n      <label class=\"text-white\">ImageURL</label>\r\n      <input type=\"imagePath\" name=\"imagePath\" class=\"form-control\" [(ngModel)]=\"userToAdd.imagePath\" placeholder=\"imagePath\"/>\r\n  </div>\r\n  <br/>\r\n  <input class=\"btn btn-primary col-md-12\" type=\"submit\" value=\"Register\"/>\r\n  <br/>\r\n  <br/>\r\n</form>\r\n<app-verify class=\"d-none\" [userToAdd]=\"userToAdd\"></app-verify>"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var rxjs_1 = __webpack_require__("./node_modules/rxjs/Rx.js");
var RegisterComponent = (function () {
    function RegisterComponent(http, router) {
        this.http = http;
        this.router = router;
        this.path = 'api/user';
        this.selectedFile = null;
        this.userToAdd = {
            username: '',
            password: '',
            firstName: '',
            lastName: '',
            imagePath: '',
            email: '',
            locked: false,
            role: 'USER'
        };
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.resetUser();
    };
    RegisterComponent.prototype.addUser = function () {
        var _this = this;
        console.log(this.userToAdd);
        this.http.post(this.path, this.userToAdd).subscribe(function (res) {
            _this.router.navigate(['/verification']);
            _this.resetUser();
        });
    };
    RegisterComponent.prototype.resetUser = function () {
        this.userToAdd = {
            username: '',
            password: '',
            firstName: '',
            lastName: '',
            imagePath: '',
            email: '',
            locked: false,
            role: 'USER'
        };
    };
    RegisterComponent.prototype.upload = function (event) {
        this.selectedFile = event.target.files[0];
    };
    RegisterComponent.prototype.onUpload = function () {
        var fd = new FormData();
        fd.append('image', this.selectedFile, this.selectedFile.name);
        this.http.post('https://localhost:8080/upload', fd).subscribe(function (res) {
            console.log(res);
        });
    };
    RegisterComponent.prototype.fileChange = function (event) {
        var fileList = event.target.files;
        if (fileList.length > 0) {
            var file = fileList[0];
            var formData = new FormData();
            formData.append('file', file, file.name);
            this.http.post('https://localhost:8080/upload', formData)
                .catch(function (error) { return rxjs_1.Observable.throw(error); })
                .subscribe(function (data) { return console.log(data); }, function (error) { return console.log(error); });
        }
    };
    RegisterComponent = __decorate([
        core_1.Component({
            selector: 'app-register',
            template: __webpack_require__("./src/app/register/register.component.html"),
            styles: [__webpack_require__("./src/app/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.Router])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;


/***/ }),

/***/ "./src/app/service/authentication.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var Rx_1 = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var jwt_utils_service_1 = __webpack_require__("./src/app/service/jwt-utils.service.ts");
var AuthenticationService = (function () {
    function AuthenticationService(http, jwtUtilsService) {
        this.http = http;
        this.jwtUtilsService = jwtUtilsService;
        this.loginPath = '/api/login';
    }
    AuthenticationService.prototype.login = function (username, password) {
        var _this = this;
        var headers = new http_1.HttpHeaders({ 'Content-Type': 'application/json' });
        return this.http.post(this.loginPath, JSON.stringify({ username: username, password: password }), { headers: headers })
            .map(function (res) {
            var token = res && res['token'];
            if (token) {
                localStorage.setItem('currentUser', JSON.stringify({
                    username: username,
                    roles: _this.jwtUtilsService.getRoles(token),
                    token: token
                }));
                return true;
            }
            else {
                return false;
            }
        })
            .catch(function (error) {
            if (error.status === 400) {
                return Rx_1.Observable.throw('Ilegal login');
            }
            else {
                return Rx_1.Observable.throw(error.json().error || 'Server error');
            }
        });
    };
    AuthenticationService.prototype.getToken = function () {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var token = currentUser && currentUser.token;
        return token ? token : "";
    };
    AuthenticationService.prototype.logout = function () {
        localStorage.removeItem('currentUser');
    };
    AuthenticationService.prototype.isLoggedIn = function () {
        if (this.getToken() != '')
            return true;
        else
            return false;
    };
    AuthenticationService.prototype.getCurrentUser = function () {
        if (localStorage.currentUser) {
            return JSON.parse(localStorage.currentUser);
        }
        else {
            return undefined;
        }
    };
    AuthenticationService.prototype.isAdmin = function () {
        return this.getCurrentUser().role.indexOf('ADMINISTRATOR') >= 0;
    };
    AuthenticationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, jwt_utils_service_1.JwtUtilsService])
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;


/***/ }),

/***/ "./src/app/service/can-activate-auth.guard.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var authentication_service_1 = __webpack_require__("./src/app/service/authentication.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var CanActivateAuthGuard = (function () {
    function CanActivateAuthGuard(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
    }
    CanActivateAuthGuard.prototype.canActivate = function (next, state) {
        if (this.authenticationService.isLoggedIn()) {
            return true;
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    };
    CanActivateAuthGuard = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService, router_1.Router])
    ], CanActivateAuthGuard);
    return CanActivateAuthGuard;
}());
exports.CanActivateAuthGuard = CanActivateAuthGuard;


/***/ }),

/***/ "./src/app/service/jwt-utils.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var JwtUtilsService = (function () {
    function JwtUtilsService() {
    }
    JwtUtilsService.prototype.getRoles = function (token) {
        var jwtData = token.split('.')[1];
        var decodedJwtJsonData = window.atob(jwtData);
        var decodedJwtData = JSON.parse(decodedJwtJsonData);
        return decodedJwtData.roles.map(function (x) { return x.authority; }) || [];
    };
    JwtUtilsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], JwtUtilsService);
    return JwtUtilsService;
}());
exports.JwtUtilsService = JwtUtilsService;


/***/ }),

/***/ "./src/app/service/token-interceptor.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var authentication_service_1 = __webpack_require__("./src/app/service/authentication.service.ts");
var core_2 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var TokenInterceptorService = (function () {
    function TokenInterceptorService(inj) {
        this.inj = inj;
    }
    TokenInterceptorService.prototype.intercept = function (request, next) {
        var authenticationService = this.inj.get(authentication_service_1.AuthenticationService);
        request = request.clone({
            setHeaders: {
                'X-Auth-Token': "" + authenticationService.getToken()
            }
        });
        return next.handle(request);
    };
    TokenInterceptorService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [core_2.Injector])
    ], TokenInterceptorService);
    return TokenInterceptorService;
}());
exports.TokenInterceptorService = TokenInterceptorService;


/***/ }),

/***/ "./src/app/user-preview/user-preview.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user-preview/user-preview.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\" *ngIf=\"user\">\n    <div class=\"col-md-2\">\n      <img [src]=\"getSantizeUrl(user.imagePath)\" width=\"250px\" height=\"200px\">\n    </div>\n    <div class=\"col-md-1\"></div>\n    <div class=\"col-md-9\">\n      <h2 class=\"text-white\">First Name: {{user.firstName}}</h2>\n      <h2 class=\"text-white\">Last Name: {{user.lastName}}</h2>\n      <br/>\n      <h4 class=\"text-white\">E-mail: {{user.email}}</h4>\n      <h4 class=\"text-white\">Role: {{user.role}}</h4>\n    </div>  \n  </div>\n</div>"

/***/ }),

/***/ "./src/app/user-preview/user-preview.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var platform_browser_1 = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var UserPreviewComponent = (function () {
    function UserPreviewComponent(route, http, sanitizer) {
        this.route = route;
        this.http = http;
        this.sanitizer = sanitizer;
    }
    UserPreviewComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    UserPreviewComponent.prototype.loadData = function () {
        var _this = this;
        this.route.params.subscribe(function (param) {
            _this.id = +param['id'];
            _this.http.get("api/users/" + _this.id).subscribe(function (res) {
                _this.user = res;
            });
        });
    };
    UserPreviewComponent.prototype.getSantizeUrl = function (url) {
        return this.sanitizer.bypassSecurityTrustUrl(url);
    };
    UserPreviewComponent = __decorate([
        core_1.Component({
            selector: 'app-user-preview',
            template: __webpack_require__("./src/app/user-preview/user-preview.component.html"),
            styles: [__webpack_require__("./src/app/user-preview/user-preview.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute, http_1.HttpClient, platform_browser_1.DomSanitizer])
    ], UserPreviewComponent);
    return UserPreviewComponent;
}());
exports.UserPreviewComponent = UserPreviewComponent;


/***/ }),

/***/ "./src/app/verification-message/verification-message.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/verification-message/verification-message.component.html":
/***/ (function(module, exports) {

module.exports = "<div align=\"center\">\n  <h4 class=\"text-white\">\n    Your verification message was sent to your e-mail!\n  </h4>\n  <h4 class=\"text-white\">\n    Please, click on the sent link to verify your account.\n  </h4>\n</div>\n\n"

/***/ }),

/***/ "./src/app/verification-message/verification-message.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var VerificationMessageComponent = (function () {
    function VerificationMessageComponent() {
    }
    VerificationMessageComponent.prototype.ngOnInit = function () {
    };
    VerificationMessageComponent = __decorate([
        core_1.Component({
            selector: 'app-verification-message',
            template: __webpack_require__("./src/app/verification-message/verification-message.component.html"),
            styles: [__webpack_require__("./src/app/verification-message/verification-message.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], VerificationMessageComponent);
    return VerificationMessageComponent;
}());
exports.VerificationMessageComponent = VerificationMessageComponent;


/***/ }),

/***/ "./src/app/verify/verify.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/verify/verify.component.html":
/***/ (function(module, exports) {

module.exports = "<h1 class=\"text-white\">\n  You have successfully verified your account!\n</h1>\n"

/***/ }),

/***/ "./src/app/verify/verify.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var VerifyComponent = (function () {
    function VerifyComponent(httpClient) {
        this.httpClient = httpClient;
        this.path = 'confirmationRegistration';
    }
    VerifyComponent.prototype.ngOnInit = function () {
        this.verifyUser(this.userToAdd.id);
    };
    VerifyComponent.prototype.verifyUser = function (id) {
        return this.httpClient.get(this.path + '/' + id);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], VerifyComponent.prototype, "userToAdd", void 0);
    VerifyComponent = __decorate([
        core_1.Component({
            selector: 'app-verify',
            template: __webpack_require__("./src/app/verify/verify.component.html"),
            styles: [__webpack_require__("./src/app/verify/verify.component.css")]
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], VerifyComponent);
    return VerifyComponent;
}());
exports.VerifyComponent = VerifyComponent;


/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
__webpack_require__("./node_modules/hammerjs/hammer.js");
var app_module_1 = __webpack_require__("./src/app/app.module.ts");
var environment_1 = __webpack_require__("./src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map