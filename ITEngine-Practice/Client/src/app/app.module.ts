import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { Routes, RouterModule } from '@angular/router';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './service/token-interceptor.service';
import { AuthenticationService } from './service/authentication.service';
import { JwtUtilsService } from './service/jwt-utils.service';
import { CanActivateAuthGuard } from './service/can-activate-auth.guard';
import { UserService } from './main/user.service';
import { UserPreviewComponent } from './user-preview/user-preview.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalComponent } from './modal/modal.component';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { VerificationMessageComponent } from './verification-message/verification-message.component';
import { VerifyComponent } from './verify/verify.component';



const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'verification', component: VerificationMessageComponent },
  { path: 'main', component: MainComponent, canActivate:[CanActivateAuthGuard] },
  { path: 'users/:id', component: UserPreviewComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'confirmationRegistration', component: VerifyComponent },
  { path: 'lazy', component: ModalComponent },
  { path: '', redirectTo: 'main', pathMatch: 'full', canActivate:[CanActivateAuthGuard] },
  { path: '**', component: PagenotfoundComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    PagenotfoundComponent,
    LoginComponent,
    MainComponent,
    UserPreviewComponent,
    EditUserComponent,
    ModalComponent,
    VerificationMessageComponent,
    VerifyComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    MatButtonModule, 
    MatCheckboxModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    ToastModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false
      }
    )
  ],
  entryComponents: [
    ModalComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    AuthenticationService,
    CanActivateAuthGuard,
    JwtUtilsService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
