import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../model/user';

@Injectable()
export class UserService {
  readonly path = 'api/users';

  constructor(private httpClient: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.httpClient.get(this.path) as Observable<User[]>;
  }

  getUser(id: number) {
    return this.httpClient.get(this.path + '/' + id);
  }

  editUser(user: User) {
    return this.httpClient.put(this.path + '/' + user.id, user)
  }

  deleteNews(user: User) {
    return this.httpClient.delete(this.path + '/' + user.id);
  }

  findByTitle(title: string): Observable<User[]> {
    return this.httpClient.get(this.path + '?firstName=' + title) as Observable<User[]>;
  }

}
