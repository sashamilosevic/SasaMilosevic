import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { User } from '../model/user';
import { UserService } from '../main/user.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  @ViewChild(ModalComponent) modal: ModalComponent;

  modalRef:NgbModalRef;

  // modal: ModalComponent;

  users: User[];

  id:number;

  public isAdministrator: boolean;

  show:boolean = false;

  forEdit: any = this.userService.getUser(this.id);;

  image: string = 'C:\SasaMilosevic\Workspace\ITEngine\Client\src\assets\download.jpg)';
  trustedUrl:any;

  title:string;

  constructor(public toastr: ToastsManager, vcr:ViewContainerRef ,private modalService: NgbModal, private http: HttpClient, private authenticationService: AuthenticationService, private userService: UserService, private sanitizer:DomSanitizer, private router: Router) { 
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.loadData();
    // this.isAdministrator = this.authenticationService.isAdmin();
    this.trustedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.image);
  }

  loadData() {
    this.userService.getUsers().subscribe(data=> {
      this.users = data;
    })
  }

  editUser(user: User) {
    this.userService.editUser(user).subscribe(data=> {
      this.loadData();
    })
  }

  delete(user: User) {
    this.userService.deleteNews(user).subscribe(()=> {
      this.loadData();
    })
  }

  isLoggedIn(): boolean {
    return this.authenticationService.isLoggedIn();
  }

  public getSantizeUrl(url : string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  toggle() {
    this.show = !this.show;
  }

  openModal()  {
    this.modalRef = this.modalService.open(this.modal);
  }

  showSuccess() {
    this.toastr.error('User deleted.', 'Success!');
  }

  searchByTitle() {
    this.userService.findByTitle(this.title).subscribe(data=> {
      this.users = data;
    })
  }

  showInfo() {
    this.toastr.info('User edited.');
  }
  
}
