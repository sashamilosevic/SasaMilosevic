export interface User{
    id?: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    imagePath: string;
    email: string;
    locked: boolean;
    role: string;
}