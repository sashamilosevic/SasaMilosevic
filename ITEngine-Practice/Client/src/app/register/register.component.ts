import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  readonly path = 'api/user';
  selectedFile: File = null;

  userToAdd: User = {
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    imagePath:'',
    email:'',
    locked:false,
    role:'USER'
  }

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.resetUser();
  }

  addUser() {
    console.log(this.userToAdd);
    this.http.post(this.path, this.userToAdd).subscribe(
      (res: Response) => {
        this.router.navigate(['/verification']);           
        this.resetUser();
      })
  }

  resetUser() {
    this.userToAdd = {
      username: '',
      password: '',
      firstName: '',
      lastName: '',
      imagePath:'',
      email:'',
      locked:false,
      role:'USER'
    }
  }

  upload(event): void {
    this.selectedFile = <File>event.target.files[0];
  }

  onUpload() {
    const fd = new FormData();
    fd.append('image', this.selectedFile, this.selectedFile.name);
    this.http.post('https://localhost:8080/upload',fd).subscribe(res=> {
      console.log(res);
    });
  }

  fileChange(event): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];

      const formData = new FormData();
      formData.append('file', file, file.name);

      this.http.post('https://localhost:8080/upload', formData)
        .catch(error => Observable.throw(error))
        .subscribe(
        data => console.log(data),
        error => console.log(error)
        );
    }
  }

}