import { Component, OnInit, Input } from '@angular/core';
import { User } from '../model/user';
import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.css']
})
export class VerifyComponent implements OnInit {

  readonly path = 'confirmationRegistration';

  @Input()
  userToAdd: User;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    this.verifyUser(this.userToAdd.id);
  }

  verifyUser(id: number) {
    return this.httpClient.get(this.path + '/' + id);
  }

}
