import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef} from '@angular/material';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { User } from '../model/user';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  closeResult: string;

  @Input()
  user: User;

  @Output()
  userDeleted: EventEmitter<User> = new EventEmitter()

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  delete() {
    this.userDeleted.next(this.user);
  }

 

}
