import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Http, Response, RequestOptions, 
  Headers, URLSearchParams } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-user-preview',
  templateUrl: './user-preview.component.html',
  styleUrls: ['./user-preview.component.css']
})
export class UserPreviewComponent implements OnInit {

  user:User;
  private id: number;

  constructor(private route: ActivatedRoute, private http: HttpClient, private sanitizer:DomSanitizer) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.route.params.subscribe(param => {
      this.id = +param['id'];
      this.http.get(`api/users/${this.id}`).subscribe(
        (res: any) => {
          this.user = res;
        }
      )
    })
  }

  public getSantizeUrl(url : string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
}

}
