import { Component, OnInit, Input, Output, EventEmitter, ViewContainerRef } from '@angular/core';
import { User } from '../model/user';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  @Input()
  id: number;

  @Input()
  user: User;

  @Output() 
  editedStudent: EventEmitter<User> = new EventEmitter();

  show:boolean = false;

  constructor(private http: HttpClient, public toastr: ToastsManager, vcr:ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
   }

  ngOnInit() {
    
  }

  editUser() {
    this.editedStudent.next(this.user);
  }

  toggle() {
    this.show = !this.show;
  }

  alert() {
    alert(this.user.firstName + " " + this.user.lastName + " is changed")
  }

  showInfo() {
    this.toastr.info('User edited.');
  }

}
