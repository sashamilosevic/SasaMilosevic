import { Component, OnInit } from '@angular/core';
import { RequestOptions, Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {

  constructor(public http: Http) { }

  fileChange(event): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];

      const formData = new FormData();
      formData.append('file', file, file.name);

      const headers = new Headers();
      // It is very important to leave the Content-Type empty
      // do not use headers.append('Content-Type', 'multipart/form-data');
      headers.append('Authorization', 'Bearer ' + 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....');
      const options = new RequestOptions({ headers: headers });

      this.http.post('https://localhost:8080/upload', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
        data => console.log(data),
        error => console.log(error)
        );
    }
  }

  ngOnInit() {
  }

}
