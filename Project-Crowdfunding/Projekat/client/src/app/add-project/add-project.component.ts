import { Component, OnInit } from '@angular/core';
import { Project } from '../model/project';
import { ProjectService } from '../service/project.service';
import { AuthenticationService } from '../service/authentication-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit {

  project: Project;
  categories: any;
  constructor(private projectService: ProjectService, private router: Router) { }

  ngOnInit() {
    this.resetProjectToAdd();
    this.categories = [
      { name: "Art", value: "ART" },
      { name: "Comics", value: "COMICS" },
      { name: "Design", value: "DESIGN" },
      { name: "Fashion", value: "FASHION" },
      { name: "Food", value: "FOOD" },
      { name: "Games", value: "GAMES" },
      { name: "Music", value: "MUSIC" },
      { name: "Techonology", value: "TECHNOLOGY" }];
  }

  addProject() {
    this.projectService.saveProject(this.project);
    this.resetProjectToAdd();
    this.router.navigate(['/main']);
  }

  resetProjectToAdd() {
    this.project = {
      name: '',
      category: '',
      image: '',
      goal: 0,
      raised: 0,
      about: ''
    }
  }
}
