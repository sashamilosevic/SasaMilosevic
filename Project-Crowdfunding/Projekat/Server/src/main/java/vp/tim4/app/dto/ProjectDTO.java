package vp.tim4.app.dto;

import java.util.List;
import vp.tim4.app.model.Category;
import vp.tim4.app.model.ChatItem;
import vp.tim4.app.model.Project;
import vp.tim4.app.model.ProjectRoles;
import vp.tim4.app.model.SecurityUser;

public class ProjectDTO {

	private Long id;
	private String name;
	private Category category;
	private String imagePath;
	private double goal;
	private double raised;
	private String about;
	private SecurityUser owner;
	private List<ChatItem> chatItems;
	private List<ProjectRoles> projectRole;

	public ProjectDTO() {

	}

	public ProjectDTO(Project project) {
		this.id = project.getId();
		this.name = project.getName();
		this.category = project.getCategory();
		this.imagePath = project.getImagePath();
		this.goal = project.getGoal();
		this.raised = project.getRaised();
		this.about = project.getAbout();
		this.owner = project.getOwner();
	}

	public ProjectDTO(Long id, String name, Category category, String imagePath, double goal, double raised,
			String about, SecurityUser owner, List<ChatItem> chatItems, List<ProjectRoles> projectRole) {
		super();
		this.id = id;
		this.name = name;
		this.category = category;
		this.imagePath = imagePath;
		this.goal = goal;
		this.raised = raised;
		this.about = about;
		this.owner = owner;
		this.chatItems = chatItems;
		this.projectRole = projectRole;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public double getGoal() {
		return goal;
	}

	public void setGoal(double goal) {
		this.goal = goal;
	}

	public double getRaised() {
		return raised;
	}

	public void setRaised(double raised) {
		this.raised = raised;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public SecurityUser getOwner() {
		return owner;
	}

	public void setOwner(SecurityUser owner) {
		this.owner = owner;
	}

	public List<ChatItem> getChatItems() {
		return chatItems;
	}

	public void setChatItems(List<ChatItem> chatItems) {
		this.chatItems = chatItems;
	}

	public List<ProjectRoles> getProjectRole() {
		return projectRole;
	}

	public void setProjectRole(List<ProjectRoles> projectRole) {
		this.projectRole = projectRole;
	}
}
