package vp.tim4.app.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@CrossOrigin(origins = "*")
@RestController
public class ImageUploadController {
	@PostMapping(value = "/upload")
	public ResponseEntity<String> UploadFile(MultipartHttpServletRequest request) throws IOException {

		Iterator<String> itr = request.getFileNames();
		MultipartFile file = request.getFile(itr.next());
		String fileName = file.getOriginalFilename();
		File dir = new File("/src/main/resources/images/");// cuvamo slike na serveru
		String imagePath = "/src/main/resources/images/" + fileName;
		if (dir.isDirectory()) {
			File serverFile = new File(dir, fileName);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(file.getBytes());
			stream.close();
		}
		return new ResponseEntity<>(imagePath, HttpStatus.OK);
	}

}
